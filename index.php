<?php

require_once dirname(__FILE__) . '/vendor/autoload.php';
require_once dirname(__FILE__) . '/Errors.php';

use Lift\Lift;
use Lift\LiftController;

$lift = new LiftController();
$lift->init();
$lift->start();
