<?php

set_error_handler('error_handler',E_ALL);

function error_handler($errno,$message,$file,$line){

    try {
        switch ($errno) {
            case E_WARNING;
                $message = $message . ' ' . $file . ' on line' .  $line;
                throw new WarningException($message);
                break;
            case E_NOTICE;
                $message = $message . ' ' . $file . ' on line' .  $line;
                throw new NoticeException($message);
                break;
            default;
                $message = $message . ' ' . $file . ' on line' .  $line;
                throw new Exception($message);
        }
    }catch (Exception $e){
        echo "Custom exception {$e->getMessage()}" . PHP_EOL;
    }
}

class WarningException extends Exception{

    public function __toString()
    {
        return "Custom warning {$this->message}" . PHP_EOL;
    }
}

class NoticeException extends Exception{

    public function __toString()
    {
        return "Custom notice {$this->message} " . PHP_EOL;
    }
}
