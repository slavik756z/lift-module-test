<?php
namespace Lift;

use Lift\Lift;

Class LiftController {

    const LIFT_BEGIN_STAGE = 1; // - начальное положение лифта
    const LIFTS = 2; // - количество шахт лифта

    public $totalPersons; // - количество всех пассажиров
    /**
     * @var array Lift
     */
    public $lift;

    /**
     * Инициализация пассажиров и лифтов
     *
     * @return bool
     */
    public function init()
    {
        echo 'Init lift' . PHP_EOL;
        echo 'Please enter file with persons ... For example:"test1.json","test2.json","test3.json"' . PHP_EOL;
        while(true) {
            try {
                $file = trim(fgets(STDIN));
                $this->totalPersons = json_decode(file_get_contents(dirname(__DIR__) . '/persons/' . $file));
                break;
            }catch (\Exception $e){
                echo 'Incorrect file name ' . PHP_EOL;
                continue;
            }
        }
        for ($i = 0;$i < self::LIFTS;$i++){
            $this->lift[] = new Lift(self::LIFT_BEGIN_STAGE);
        }

        echo 'Total persons in stack = ' . count($this->totalPersons) . PHP_EOL;
        return true;
    }

    public function start()
    {
        echo "---START---" . PHP_EOL;

        while (true){

            foreach ($this->lift as $key => $lift) {
                echo '-----------------' . PHP_EOL;
                echo "Lift {$key} on stage " . $lift->stage . PHP_EOL;

                $this->totalPersons = $lift->outPersonsFromLift($this->totalPersons);
                $this->totalPersons = $lift->addPersonsToLift($this->totalPersons);
                $lift->move();
            }

            if(empty($this->totalPersons)){
                echo "Lift work has been finished!";
                break;
            }

            sleep(1);
        }
        return null;
    }
}