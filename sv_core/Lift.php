<?php

namespace Lift;

Class Lift{

    const PERSONS_LIMIT = 2; // - лимит людей в кабине лифта
    const STAGES_LIMIT = 5; // - лимит этажей
    const FIRST_STAGE = 1; // минимальный этаж

    public $stage; // - текущий этаж лифта
    public $persons; // - текущие количество людей в кабине
    public $finalStage; // - пунк назначени лифта
    public $vip_mode; // - вип режим

    public function __construct($stage)
    {
        $this->stage = $stage; // - задаем начальное положение лифта
    }

    /**
     * Посадка пассажиров в кабинку лифта
     *
     * @param $totalPersons
     * @return array
     */
    public function addPersonsToLift($totalPersons)
    {
        if (!empty($totalPersons)) {
            if($this->vip_mode == false) {
                foreach ($totalPersons as $key => $person) {

                    if(!$this->validatePerson($person)){ // - Проверка наличия максимального этажа,и минимального,и не нажат ли один и тот же этаж.
                        echo 'Person ' . $person->name . 'invalid!' . PHP_EOL;
                        unset($totalPersons[$key]);
                        continue;
                    }

                    if ($person->stage == $this->stage && count($this->persons) < self::PERSONS_LIMIT && !isset($totalPersons[$key]->status)) {

                        If ($person->vip == true) { // - вип режим
                            $this->vip_mode = true;
                        }
                        $this->persons[$key] = $person;

                        $totalPersons[$key]->status = true;

                        echo "Person {$person->name} - sit to lift!" . PHP_EOL;
                    }
                }
            }else{
                echo "Vip mode - move next" . PHP_EOL;
            }
            $this->getFinalStage($totalPersons); // - нахождение конечного пункта лифта

            return $totalPersons;
        }

        return [];
    }

    /**
     * Высадка людей и очистка из глобального списка
     *
     * @param $totalPersons
     * @return mixed
     */
    public function outPersonsFromLift($totalPersons)
    {
        if (!empty($this->persons)) {
            $personsInLift = $this->persons;
            foreach ($personsInLift as $key => $person) {
                if ($person->finish == $this->stage) {
                    If($person->vip == true){ // - выключение вип режима
                        $this->vip_mode = false;
                    }
                    unset($this->persons[$key]);
                    unset($totalPersons[$key]);
                    echo "Person {$person->name} - exit!" . PHP_EOL;
                }
            }
        }
        return $totalPersons;
    }

    /**
     * Пункт назначения
     *
     * @param $totalPersons
     * @return bool|int|string
     */
    public function getFinalStage($totalPersons)
    {
        if(!empty($this->persons)){
            $stage = 0;
            foreach ($this->persons as $person){
                if($stage < $person->finish){
                    $stage = $person->finish;
                }
            }
            $this->finalStage = $stage;
            return $this->finalStage;
        }
        return $this->getNearPerson($totalPersons); // - нахождения этажа куда ехать если в кабине нет пассажиров
    }

    /**
     * Движение лифта
     */
    public function move()
    {
        if($this->finalStage > $this->stage){
            $this->stage++;
        }elseif($this->finalStage < $this->stage){
            $this->stage--;
        }
    }

    /**
     * Нахождение ближнего пассажира
     *
     * @param $totalPersons
     * @return bool|int|string
     */
    public function getNearPerson($totalPersons)
    {
        if(!empty($totalPersons)){
            $this->finalStage = $this->searchNearest($totalPersons);
            return $this->finalStage;
        }
        return false;
    }

    /**
     * Поиск ближнего этажа
     *
     * @param $totalPersons
     * @return int|string
     */
    private function searchNearest($totalPersons)
    {
        $lastKey = null;
        $lastDif = null;
        foreach ($totalPersons as $k => $person) {
            if (!isset($totalPersons[$k]->status) && $this->validatePerson($person)){
                if ($person->stage == $this->stage) {
                    return $k;
                }
                $dif = abs($this->stage - $person->stage);
                if (is_null($lastKey) || $dif < $lastDif) {
                    $lastKey = $k;
                    $lastDif = $dif;
                }
            }
        }

        if (isset($totalPersons[$lastKey])) {
            echo 'Nearest stage ' . $totalPersons[$lastKey]->stage . PHP_EOL;
            return $totalPersons[$lastKey]->stage;
        }else{
            echo 'Stop work on stage ' . $this->stage . PHP_EOL;
            return $this->stage;
        }
    }

    /**
     * Валидация пассажиров
     *
     * @param $person
     * @return bool
     */
    private function validatePerson($person)
    {
        if($person->stage == $person->finish || $person->stage > self::STAGES_LIMIT || $person->finish > self::STAGES_LIMIT || $person->stage < self::FIRST_STAGE || $person->finish < self::FIRST_STAGE){
            return false;
        }
        return true;
    }
}